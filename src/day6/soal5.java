package day6;

import java.util.Scanner;

public class soal5 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan nilai : ");
		int nilai = input.nextInt(); //input nilai awal
		int prima = 2; // inisialisasi bilangan prima
		int result = 0; // hasil
		while (nilai != 1) { //jika 7 tidak sama dengan 1 karena bilangan prima tidak di mulai dari 1
			result = nilai; // variabel result di masukan nilai dari nilai
			if (result % prima == 0) {
				result = nilai / prima;
				System.out.println("" + nilai + "/" + prima + "=" + result);
				nilai = result;
			} else {
				prima++;
			}
		}
	}

}
