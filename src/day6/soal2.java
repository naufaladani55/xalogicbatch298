package day6;

import java.util.Scanner;

public class soal2 {
//soal2 done
	public static void main(String[] args) {
		// 1,1,1,3,5,9,17

		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Panjang Array : ");
		int number = input.nextInt();

		int[] array = new int[number];

		int nilai1 = 1, nilai2 = 1, nilai3 = 1, nilai4 = 0; // inisialisasi variabel nilai1 , nilai 2 , nilai 3 , nilai
															// 4

//		System.out.print(1 + " ");
		//variabel i kurang dari 7 maka variabel i+1
		for (int i = 0; i < array.length; i++) { // inisialisasi variabel i = 0
			if (i < 3) { // jika 0 kurng dari 3 maka
				System.out.print(nilai1 + " "); // tampilkan isi nilai dari variabel nilai1
				continue; // apapun yang di bawah continue tidak akan di eksekusi tapi program tetap jalan
			}
			//proses dimulai perulangan i ke 4

			nilai4 = nilai1 + nilai2 + nilai3; // nilai4 = 1 + 1 + 1
			System.out.print(nilai4 + " "); // print 3 + " "    note: cetak hasil dari nilai4 + spasi
			nilai1 = nilai2; // nilai1 = (1)   note :isi dari variabel nilai2 
			nilai2 = nilai3; // nilai2 = (1)   note :isi dari variabel nilai3 
			nilai3 = nilai4; // nilai3 = (3)   note :isi dari variabel nilai4 

		}

		System.out.println();
	}

}
