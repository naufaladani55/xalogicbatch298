package day6;

import java.util.Arrays;
import java.util.Scanner;

public class MainFibonaci {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				// jika case 1 terdapat soal 1 maka kerjakan soal 1
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {// Keterangan :
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukan Angka : ");
		int angka = input.nextInt(); // input angka

		int[] array = new int[angka];
		int nilai1 = 0, nilai2 = 1, nilai3 = 0;

//		System.out.print(nilai2+" ");
		for (int i = 1; i < array.length; i++) { // 0 < panjang fibonaci = 7

			if (i == 1) {
				System.out.print(i + " ");
			}

			nilai3 = nilai1 + nilai2;

			System.out.print(nilai3 + " ");

			nilai1 = nilai2;
			nilai2 = nilai3;

		}
		System.out.println();
	}

	private static void soal2() { // Keterangan :
		System.out.println("Disini Soal 2");
		System.out.println(
				"===================================================================================================");

		System.out.println("*Soal 2 - Fibonaci ke-2*");
		System.out.println();
		System.out.println("Masukan Panjang Array : ");
		int number = input.nextInt();

		int[] array = new int[number];

		int nilai1 = 1, nilai2 = 1, nilai3 = 1, nilai4 = 0; // inisialisasi variabel nilai1 , nilai 2 , nilai 3 , nilai
															// 4

//		System.out.print(1 + " ");
		// variabel i kurang dari 7 maka variabel i+1
		for (int i = 0; i < array.length; i++) { // inisialisasi variabel i = 0
			if (i < 3) { // jika 0 kurng dari 3 maka
				System.out.print(nilai1 + " "); // tampilkan isi nilai dari variabel nilai1
				continue; // apapun yang di bawah continue tidak akan di eksekusi tapi program tetap jalan
			}
			// proses dimulai perulangan i ke 4

			nilai4 = nilai1 + nilai2 + nilai3; // nilai4 = 1 + 1 + 1
			System.out.print(nilai4 + " "); // print 3 + " " note: cetak hasil dari nilai4 + spasi
			nilai1 = nilai2; // nilai1 = (1) note :isi dari variabel nilai2
			nilai2 = nilai3; // nilai2 = (1) note :isi dari variabel nilai3
			nilai3 = nilai4; // nilai3 = (3) note :isi dari variabel nilai4

		}

		System.out.println();
	}

	private static void soal3() {// Keterangan :
		System.out.println("Disini Soal 3");
		System.out.println(
				"===================================================================================================");
		System.out.println("*Soal 3 - Bilangan Prima*");
		System.out.println();

		System.out.println("Masukkan Nilai N: ");
		int inputP = input.nextInt();

		String hasil = "";

		for (int i = 2; i < inputP; i++) {
			int status = 0;
			for (int j = 2; j < i; j++) {

				if (i % j == 0) {
					status++;
				}

			}

			if (status == 0) {
				hasil += i + ",";
			}

		}
		System.out.println(hasil.substring(0, hasil.length() - 1));

	}

	private static void soal4() {// Keterangan :
		System.out.println("Disini Soal 4");
		System.out.println(
				"===================================================================================================");
		System.out.println("*Soal 4 - Time Conversion*");
		System.out.println();

		System.out.print("Masukkan Waktu yang akan dikonversi (HH:MM:SS[AM/PM]): ");
		String inputWaktu = input.next();

		if (inputWaktu.length() != 10) {
			System.out.println("Maaf, waktu yang anda masukkan salah karena tidak memenuhi kaidah AM/PM");
			return;
		}

		String waktu = inputWaktu.substring(0, 8);
		String[] waktuStr = waktu.split(":");
		int[] waktuInt = new int[waktuStr.length];
		String hasil = "";
		String ampm = inputWaktu.substring(8, 10).toLowerCase();

		for (int i = 0; i < waktuStr.length; i++) {
			waktuInt[i] = Integer.parseInt(waktuStr[i]);
		}

		if (waktuInt[0] > 0 && waktuInt[0] <= 12) {

			if (ampm.equals("pm")) {
				if (waktuInt[0] < 12 && waktuInt[0] > 0) {
					waktuInt[0] += 12;
					waktuStr[0] = "" + waktuInt[0];
				}
			} else if (ampm.equals("am")) {
				if (waktuInt[0] == 12) {
					waktuStr[0] = "00";
				}
			}
			hasil = waktuStr[0] + ":" + waktuStr[1] + ":" + waktuStr[2];

		} else {
			System.out.println("Maaf, waktu yang anda masukkan salah karena tidak memenuhi kaidah AM/PM");
		}

		System.out.println(hasil);

	}

	private static void soal5() {// Keterangan :
		System.out.println("Disini Soal 5");
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukan nilai : ");
		int nilai = input.nextInt(); // input nilai awal
		int prima = 2; // inisialisasi bilangan prima
		int result = 0; // hasil
		while (nilai != 1) { // jika 7 tidak sama dengan 1 karena bilangan prima tidak di mulai dari 1
			result = nilai; // variabel result di masukan nilai dari nilai
			if (result % prima == 0) {
				result = nilai / prima;
				System.out.println("" + nilai + "/" + prima + "=" + result);
				nilai = result;
			} else {
				prima++;
			}
		}
	}

	private static void soal6() {// Keterangan :
		System.out.println("Disini Soal 6");
		System.out.println(
				"===================================================================================================");
		System.out.println("         *Soal 6 - Perhitungan Bensin*           ");
		System.out.println("_________________________________________________");
		System.out.println("jalur 1. Jarak dari Toko ke Customer 1       = 2.0 KM");
		System.out.println("jalur 2. Jarak dari Customer 1 ke Customer 2 = 0.5 KM");
		System.out.println("jalur 3. Jarak dari Customer 2 ke Customer 3 = 1.5 KM");
		System.out.println("jalur 4. Jarak dari Customer 3 ke Customer 4 = 0.3 KM");
		System.out.println("Masukkan jalur yang ditempuh oleh driver (pisahkan dengan spasi)");
		String masukan = input.nextLine();

		String[] masukanStr = masukan.split(" ");
		int[] masukanInt = new int[masukanStr.length];
		double jarakTempuh = 0;
		double rasioBensin = 2.5;
		double[] jarak = { 2.0, 0.5, 1.5, 0.3 };

//		double[][] jarak2 = {
//				{ 0.0, 2.0, 2.5, 4.0, 4.3 },
//				{ 2.0, 0.0, 0.5, 2.0, 2.3 },
//				{ 2.5, 0.5, 0.0, 1.5, 1.8 },
//				{ 4.0, 2.0, 1.5, 0.0, 0.3 },
//				{ 4.3, 2.3, 1.8, 0.3, 0.0 }
//		};

		String hasil = "Jarak Tempuh = ";

		for (int i = 0; i < masukanStr.length; i++) {
			masukanInt[i] = Integer.parseInt(masukanStr[i]);
			if (masukanInt[i] > 4) {
				System.out.println("maaf, jalur yang anda masukkan tidak tersedia.");
				return;
			}
		}

		for (int j = 0; j < masukanInt.length; j++) {
			jarakTempuh += jarak[masukanInt[j] - 1];
			hasil = hasil + jarak[masukanInt[j] - 1] + " KM + ";
		}

		double konsumsiBensin = Math.ceil(jarakTempuh / rasioBensin);
		hasil = hasil.substring(0, hasil.length() - 2);

		System.out.println(hasil + "= " + jarakTempuh + " KM");
		System.out.println("Bensin = " + (int) konsumsiBensin + " Liter");

	}

	private static void soal7() {// Keterangan :
		System.out.println("Disini Soal 7");
		System.out.println(
				"===================================================================================================");
		System.out.println("*Soal 7 - Sinyal SOS*");
		System.out.println("Masukkan sinyal SOS yang akan dikirimkan: ");
		String sinyalMasukan = input.next();
		sinyalMasukan = sinyalMasukan.toLowerCase();
		char sinyalChar[] = sinyalMasukan.toCharArray();
		int salah = 0;
		for (int i = 0; i < sinyalMasukan.length(); i += 3) {

			if (sinyalChar[i] != 's') {
				salah++;
			}
			if (sinyalChar[i + 1] != 'o') {
				salah++;
			}
			if (sinyalChar[i + 2] != 's') {
				salah++;
			}

		}
		System.out.println("Sehingga total sinyal yang salah adalah: " + salah);

	}

	private static void soal8() {// Keterangan :
		System.out.println("Disini Soal 8");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal9() {// Keterangan :
		System.out.println("Disini Soal 9");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal10() {// Keterangan :
		System.out.println("Disini Soal 10");
		System.out.println(
				"===================================================================================================");

	}
}
