package PreTest;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				// jika case 1 terdapat soal 1 maka kerjakan soal 1
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {// Keterangan :
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukan Angka");
		int angka = input.nextInt();
		int genap, ganjil;
		int[] ganjilGenap = new int[angka];

		System.out.println();

		for (int i = 0; i <= ganjilGenap.length; i++) {
			ganjil = i;
			if (ganjil % 2 == 1) {

				System.out.print(ganjil + " ");
			}

		}

		System.out.println();

		for (int i = 0; i <= ganjilGenap.length; i++) {
			genap = i;

			if (genap % 2 == 0 && genap != 0) {

				System.out.print(genap + " ");
			}

		}

	}

	private static void soal2() { // Keterangan :
		System.out.println("Disini Soal 2");
		System.out.println(
				"===================================================================================================");

		System.out.print("Silahkan masukkan kata yang akan diurutkan: ");
		String kata = input.nextLine().toLowerCase().replaceAll(" ", "");
		char[] larik = kata.toCharArray();

		String vokal = "";
		String konsonan = "";

		// validasi kalau unput selain huruf salah
		for (int i = 0; i < larik.length; i++) {
			if ((int) larik[i] < 97 || (int) larik[i] > 122) {
				System.out.println("Maaf, inputan anda salah.");
				return;
			}

		}

		for (int i = 0; i < larik.length; i++) {

			if (larik[i] == 'a' || larik[i] == 'i' || larik[i] == 'u' || larik[i] == 'e' || larik[i] == 'o') {
				vokal += larik[i];
			} else {
				konsonan += larik[i];
			}
		}

		char[] vokalArr = vokal.toCharArray();
		char[] konsonanArr = konsonan.toCharArray();

		char temp = 0;
		for (int i = 0; i < vokalArr.length; i++) {
			for (int j = 0; j < vokalArr.length; j++) {
				if ((int) vokalArr[i] <= (int) vokalArr[j]) {
					temp = vokalArr[i];
					vokalArr[i] = vokalArr[j];
					vokalArr[j] = temp;
				}
			}
		}

		for (int i = 0; i < konsonanArr.length; i++) {
			for (int j = 0; j < konsonanArr.length; j++) {
				if ((int) konsonanArr[i] <= (int) konsonanArr[j]) {
					temp = konsonanArr[i];
					konsonanArr[i] = konsonanArr[j];
					konsonanArr[j] = temp;
				}
			}
		}

		System.out.println("Vokal    : " + String.valueOf(vokalArr));
		System.out.println("Konsonan : " + String.valueOf(konsonanArr));

	}

	private static void soal3() {// Keterangan :
		System.out.println("Disini Soal 3");
		System.out.println(
				"===================================================================================================");

		System.out.println("input Angka n");
		int tambah = 3;
		int nilai = 100;
		int jumlah = 0;
		int n = input.nextInt();
		int[] nArray = new int[n];

		for (int i = 1; i < nArray.length + 1; i++) {

			if (i >= i && i <= 100)
				jumlah = nilai += tambah;
			System.out.println(jumlah + "Adalah Si Angka 1");

		}
	}

	private static void soal4() {// Keterangan :
		System.out.println("Disini Soal 4"); // blm
		System.out.println(
				"===================================================================================================");
		System.out.println("jalur 1. Toko ke Tempat 1     		 = 2.0 KM");
		System.out.println("jalur 2. Jarak dari Tempat 1 ke Tempat 2 = 0.5 KM");
		System.out.println("jalur 3. Jarak dari Tempat 2 ke Tempat 3 = 1.5 KM");
		System.out.println("jalur 4. Jarak dari Tempat 3 ke Tempat 4 = 2.5 KM");
		System.out.println("Masukkan jalur yang ditempuh oleh driver (setiap jalur di kasih spasi)");
		String msk = input.nextLine();

		// convert input di pisahin spasi
		String[] inMasuk = msk.split(" ");
		int[] msktInt = new int[inMasuk.length];
		double jarakTempuh = 0;
		double rasioBensin = 2.5;
		double[] jarak = { 2.0, 0.5, 1.5, 2.5 };

		String result = "Jarak yang ditempuh: ";

		// validasi jika jarak > dari 4
		for (int i = 0; i < inMasuk.length; i++) {
			msktInt[i] = Integer.parseInt(inMasuk[i]);
			if (msktInt[i] > 5) {
				System.out.println("maaf, jalur yang anda masukkan tidak tersedia.");
				return;
			}
		}

		for (int j = 0; j < msktInt.length; j++) {
			if (inMasuk[j].equals("1") && j > 0) {

				jarakTempuh += jarakTempuh;
				result = result + jarak[msktInt[j] - 1] + " KM + ";
			} else {

				jarakTempuh += jarak[msktInt[j] - 1];
				result = result + jarak[msktInt[j] - 1] + " KM + ";
			}
		}

		double konsumsiBensin = Math.ceil(jarakTempuh / rasioBensin);
		result = result.substring(0, result.length() - 2);

		System.out.println(result + "= " + jarakTempuh + " KM");
		System.out.println("Bensin = " + (int) konsumsiBensin + " Liter");

	}

	private static void soal5() {// Keterangan :
		System.out.println("Disini Soal 5");
		System.out.println(
				"===================================================================================================");
		double[] orang = new double[5];
		System.out.println("Laki-Laki Dewasa");
		orang[0] = input.nextDouble() * 2;
		System.out.println("Wanita Dewasa");
		orang[1] = input.nextDouble() * 1;
		System.out.println("Remaja");
		orang[2] = input.nextDouble() * 2;
		System.out.println("Anak-Anak");
		orang[4] = input.nextDouble() * 0.5;
		System.out.println("Balita");
		orang[3] = input.nextDouble() * 1;

		int porsi = 0;

		for (int i = 0; i < orang.length; i++) {
			porsi += orang[i];
		}

		System.out.println(porsi + " porsi");

	}

	private static void soal6() {// Keterangan :
		System.out.println("Disini Soal 6");
		System.out.println(
				"===================================================================================================");
		System.out.println("SOAL ATM");
		int pin = input.nextInt();
		System.out.println("Uang yang disetor: ");
		BigInteger uangDiSetor = new BigInteger(input.nextInt() + "");

		System.out.println("Pilihan transfer: 1. Antar Rekening 2. Antar Bank");
		int pilihTransfer = 0;
		boolean isLoop = true;
		while (isLoop) {
			System.out.print("Masukkan angka 1 atau 2:");
			pilihTransfer = input.nextInt();
//			System.out.println(pilihTransfer);
			if (pilihTransfer == 1 || pilihTransfer == 2) {
				isLoop = false;
			}
		}

		int rekeningTujuan = 0;
		int kodeBank = 0;
		BigInteger nominalTransfer = new BigInteger("0");
		if (pilihTransfer == 1) { // Antar rekening
			System.out.println("Masukkan rekening tujuan: ");
			rekeningTujuan = input.nextInt();
			System.out.println("Masukkan nominal transfer: ");
			nominalTransfer = new BigInteger(input.nextInt() + "");
		} else { // Antar Bank
			System.out.println("masukkan kode bank: ");
			kodeBank = input.nextInt();
			System.out.println("Masukkan rekekning tujuan: ");
			rekeningTujuan = input.nextInt();
			System.out.println("Masukkan nominal transfer: ");
			nominalTransfer = new BigInteger(input.nextInt() + "");
		}

		if (nominalTransfer.compareTo(uangDiSetor) == 1) {
			System.out.println("Transaksi tidak berhasil, saldo anda saat ini Rp. " + uangDiSetor + ",-");
			return;
		}
		uangDiSetor = uangDiSetor.subtract(nominalTransfer);
		System.out.println("Tansaksi berhasil, saldo anda saat ini Rp. " + uangDiSetor + ",-");

	}

	private static void soal7() {// Keterangan :
		System.out.println("Disini Soal 7");
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukkan jumlah kartu: ");
		int jumlahKartu = input.nextInt();

		boolean gameOver = false;
		boolean surrender = false;
		while (!gameOver && !surrender) {
			System.out.println("Masukkan jumlah tawaran:");
			int jumlahTawaran = input.nextInt();

			if (jumlahTawaran > jumlahKartu) {
				System.out.println("tawaran melebihi batas");
				continue;
			}

			System.out.println("Pilih kotak 1 atau 2: ");
			boolean apaPilihKotakA = true;
			boolean isLoop = true;
			while (isLoop) {
				System.out.println("masukkan 1 atau 2:");
				int intKotak = input.nextInt();
				if (intKotak == 1) {
					apaPilihKotakA = true;
					isLoop = false;
				} else if (intKotak == 2) {
					apaPilihKotakA = false;
					isLoop = false;
				}
			}

			int[] kotakAB = new int[2];
			boolean apaMenang = false;
			boolean apaSeri = false;
			kotakAB[0] = (int) (Math.random() * 9);
			kotakAB[1] = (int) (Math.random() * 9);

			if (kotakAB[0] > kotakAB[1]) {
				if (apaPilihKotakA) {
					jumlahKartu += jumlahTawaran;
					apaMenang = true;
				} else {
					jumlahKartu -= jumlahTawaran;
					apaMenang = false;
				}
			} else if (kotakAB[1] < kotakAB[1]) {
				if (apaPilihKotakA) {
					jumlahKartu -= jumlahTawaran;
					apaMenang = false;
				} else {
					jumlahKartu += jumlahTawaran;
					apaMenang = true;
				}
			} else {
				apaSeri = true;
			}

			String menang = "";
			if (apaMenang) {
				menang = "You Win";
			} else {
				menang = "You Lose";
			}
			if (apaSeri) {
				menang = "seri";
			}

			System.out.println("Angka kotak A:" + kotakAB[0]);
			System.out.println("Angka kotak B:" + kotakAB[1]);
			System.out.println(menang);

			System.out.println("Anda menyerah???");
			input.nextLine();
			String answer = input.nextLine();
			if (answer.toLowerCase().equals("y")) {
				surrender = true;
			}

			if (jumlahKartu <= 0) {
				gameOver = true;
			}
		}

	}

	private static void soal8() {// Keterangan :
		System.out.println("Disini Soal 8");
		System.out.println(
				"===================================================================================================");
		System.out.println("Input panjang deret: ");
		int panjangDeret = input.nextInt();

		String ganjil = "";
		String genap = "";
		boolean isLoop = true;
		int n = 0;
		while (isLoop) {
			if (ganjil.split(" ").length >= panjangDeret || genap.split(" ").length >= panjangDeret) {
				isLoop = false;
			}
			if (n % 2 == 0) {
				genap += n + " ";
			} else {
				ganjil += n + " ";
			}

			n++;
		}
//		System.out.println(ganjil);
//		System.out.println(genap);

		String[] arrayGanjil = ganjil.split(" ");
		String[] arrayGenap = genap.split(" ");

		String output = "";

		for (int i = 0; i < panjangDeret; i++) {
			output += Integer.parseInt(arrayGanjil[i]) + Integer.parseInt(arrayGenap[i]) + " ";
		}
		System.out.println(output);

	}

	private static void soal9() {// Keterangan :
		System.out.println("Disini Soal 9");
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukan N dan T");
		String nilai = input.nextLine();
		String[] nilaiS = nilai.split(" ");

//		
		boolean positive = false;
		boolean negative = false;
		int gunung = 0, lembah = 0;
		int mdpl = 0;
		for (int i = 0; i < nilaiS.length; i++) {

			if (nilaiS[i].equals("N")) {

				mdpl++;
			} else {
				mdpl--;
			}

			if (mdpl > 0) {
				positive = true;
				negative = false;
			} else if (mdpl < 0) {
				negative = true;
				positive = false;
			}

			if (mdpl == 0 && positive) {
				gunung++;
			} else if (mdpl == 0 && negative) {
				lembah++;
			}
		}
		System.out.println(gunung);
		System.out.println(lembah);
//		System.out.println(mdpl);

	}

	private static void soal10() {// Keterangan :
		System.out.println("Disini Soal 10");
		System.out.println(
				"===================================================================================================");
		float promoDiskon = 0.5f;
		int maksDiskon = 100000;
		int minOrder = 40000;
		float cashback = 0.1f; // dari harga bayar
		int maksCashback = 30000;

		// System.out.print("Saldo OPO = ");
		int saldoOPO = input.nextInt();// 60000;//

		float hargaMinuman = 18000;
		int banyakMinuman = 0;
		float hasil = 0f;
		boolean isLoop = true;
		while (isLoop) {
			banyakMinuman++;
			float hargaAsli = hargaMinuman * banyakMinuman;

			// hitung diskon
			float hargaDiskon = promoDiskon * hargaAsli;
			if (hargaDiskon >= maksDiskon) {
				hargaDiskon = maksDiskon;
			}
			if (hargaAsli < minOrder) {
				hargaDiskon = 0f;
			}

			// hitung cashback
			float hargaCashback = cashback * hargaDiskon;
			if (hargaCashback >= maksCashback) {
				hargaCashback = maksCashback;
			}

			// hitung total bayar
			hasil = hargaDiskon - hargaCashback;

			if (saldoOPO - hargaMinuman < (int) hasil) {
				isLoop = false;
			}
		}

		// bayar bung!!
		saldoOPO -= hasil;
		System.out.println("Jumlah cup =  " + banyakMinuman + "; Saldo akhir = Rp. " + saldoOPO);

	}
}
