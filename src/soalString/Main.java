//HARI KE 7

package soalString;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				// jika case 1 terdapat soal 1 maka kerjakan soal 1
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {
		System.out.println("Disini Soal 1"); // Keterangan : done
		System.out.println(
				"===================================================================================================");

		Scanner input = new Scanner(System.in);

		System.out.println("Input String");
		String kalimat = input.next();

		char[] karakter = kalimat.toCharArray(); // inisalisasi string ke character

		int result = 0; // penampung untuk menghitung jumlah kata setiap ada huruf besar

		for (int i = 0; i < karakter.length; i++) {

//			System.out.print(karakter[i] + " ");
//			int ascii = 0;                           
//			if(ascii>=65 && ascii<=90 || i==0){  cara lainnya
//				result++;
//			}

			if (Character.isUpperCase(karakter[i])) {
				result++;
			}
		}
		System.out.println(result + 1); // keterangan : done tapi masih ada bug
	}

	private static void soal2() { // done tapi masih ada yg salah
		System.out.println("Disini Soal 2");
		System.out.println(
				"===================================================================================================");
		// kriteria = panjangnya minimal 6
		// katanya mengandung 1 angka
		// mengandung setidaknya 1 kata lower case
		// mengandung setidaknya 1 kata upper case
		// mengandung spesial karakter !@#$%^&*()-+
		Scanner scanner = new Scanner(System.in);

		System.out.println("Masukkan panjang Password:");
		int length = scanner.nextInt();

		System.out.println("Masukkan Password:");
		String password = scanner.next();

		int isDigit = 1, isUpper = 1, isLower = 1, isSpecial = 1, results = 0;

		for (int i = 0; i < length; i++) {
			if (Character.isDigit(password.charAt(i))) {
				isDigit = 0;
			} else if (Character.isUpperCase(password.charAt(i))) {
				isUpper = 0;
			} else if (Character.isLowerCase(password.charAt(i))) {
				isLower = 0;
			} else {
				isSpecial = 0;
			}
		}

		results = isDigit + isUpper + isLower + isSpecial;

		if (length < 6) {
			results = Math.max(length - 6, results);
		}

		System.out.println(results);

	}

	private static void soal3() { // keterangan : BELUMMMMMMMMMMMMMMMMMM
		System.out.println("Disini Soal 3");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Panjang Huruf");
		String kalimat = input.next();

		System.out.println("Masukan Kalimat");
		String kalimatInput = input.next();

		String[] panjangKalimat = kalimatInput.split("-"); // menentukan index berdasarkan kata - dan bsa ada pnjang
															// arraynya

		char[] perHuruf = kalimatInput.toCharArray(); // convert penginputan kalimatInput to Char

		System.out.println("Masukan Banyaknya Rotasi");
		int n = input.nextInt();

		for (int i = 0; i < perHuruf.length; i++) {
//			perHuruf[i] = i;
			System.out.println(perHuruf[i]);
		}
		// perubahan rotasi

		// perHuruf[0] = r2;
	}

	

	private static void soal4() {
		System.out.println("Disini Soal 4");
		System.out.println("=========================================================================");

	}

	private static void soal5() {
		System.out.println("Disini Soal 5");

	}

	private static void soal6() {
		System.out.println("Disini Soal 6");

	}
}
