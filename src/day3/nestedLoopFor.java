package day3;

public class nestedLoopFor { //LOOPING DI DALAM LOOPING

	public static void main(String[] args) {
//baris  vertical  kolom horizontal 
		for (int i=0; i<5; i++) { // baris
			for(int j=0; j<5; j++) { //kolom
				// perulangan 25x
				if(i<=j) {
					System.out.print("*");
				}
			}System.out.println();
		}
		System.out.println("===============");
		
		for (int i=0; i<5; i++) { // baris
			for(int j=0; j<5; j++) { //kolom
				// perulangan 25x
				if((i<=j) ) {
					System.out.print(" ");
				}else {
					System.out.print(" * ");
				}
			}System.out.println();
		}
		System.out.println();
	}

}
