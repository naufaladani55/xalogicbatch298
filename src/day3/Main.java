package day3;

import java.util.Scanner;


// PERULANGAN FOR

public class Main {
	
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan nilai i++ : ");	
		int j= input.nextInt();
		for (int i=0; i<j; i++) {
			System.out.println("Nilai i increment "+(i+1));
		}

		System.out.println("=========================================================");

		
		for (int i=j ;i>0 ; i--) {
			System.out.println("Nilai i decrement :" + i);
		}
		input.close();
	}
}
