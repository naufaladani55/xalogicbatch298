package day3;

import java.util.Scanner;

public class whileDo {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int n = input.nextInt();// n = 5

		int i = 0;

		do {
			if (i == n - 1) {
				System.out.print(i + 1); // 5
			} else {
				System.out.print((i + 1) + ","); // 1 , 2 , 3 , 4 ,
			}

			i++; // i = i + 1 = 1 , i = 2, i = 3, i = 4, i = 5
		} while (i < n);

	}

}
