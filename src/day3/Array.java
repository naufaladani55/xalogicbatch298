package day3;

public class Array {

	public static void Array(String[] args) {
		// TODO Auto-generated method stub

		int[] arrayAngka = new int[5];

		String dua = "2";

//		convert string to integer
		int tempAngka = Integer.parseInt(dua);

		arrayAngka[0] = 1;
//		memasukkan data ke dalam array
		arrayAngka[1] = tempAngka;
		arrayAngka[2] = 3;
		arrayAngka[3] = 4;
		arrayAngka[4] = 5;

//		akses nilai di suatu array
		int aksesAngka = arrayAngka[1];

		for (int i = 0; i < arrayAngka.length; i++) {
			System.out.print(arrayAngka[i] + " ");
		}

		System.out.println();

		System.out.println("Panjang Array " + arrayAngka.length);
		System.out.println(aksesAngka);

		String[] arrayPembelian = new String[5];

		int pembelianPertama = 10000;
		int pembelianKedua = 20000;

//		convert dari integer to string
		arrayPembelian[0] = String.valueOf(pembelianPertama);
//		cara ke-2
		arrayPembelian[1] = pembelianKedua + "";

		System.out.println("Nilai Pembelian: " + arrayPembelian[0]);

	}
}
