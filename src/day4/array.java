package day4;
// Array by Instruktur


public class array {

	public static void main(String[] args) {
		int[] arrayAngka= new int[5]; //mendefinisikan array

		String dua = "2"; // type array salah karena tidak sesuai  di baris 8
		
		int tempAngka = Integer.parseInt(dua); // convert string ke int
	
		arrayAngka[0] = 1; 
		arrayAngka[1] = tempAngka; 
		arrayAngka[2] = 3; 
		arrayAngka[3] = 4; 
		arrayAngka[4] = 5;
		
		//akses nilai di suatu array
		
		int aksesAngka = arrayAngka[1];
		
		
		for(int i=0; i < arrayAngka.length; i++) { // for untuk pengulangan  array
			System.out.println("urutan angka ke: " + (arrayAngka[i]) ); //output 1
		}
		
			System.out.println(); //output2
			System.out.println("Panjang Array " + arrayAngka.length);  //output 3
			System.out.println(aksesAngka); //output4
			
			String[] arrayPembelian = new String[5];
			
			int pembelianPertama = 10000;
			int pembelianKedua =20000;
			
			//convert dari integer to string
			arrayPembelian[0] = String.valueOf(pembelianPertama);
			//cara ke-2 
			arrayPembelian[1] = pembelianKedua + "";
		
			System.out.println("Nilai Pembelian : " + arrayPembelian[0]); //output5
			System.out.println("Nilai Pembelian : " + arrayPembelian[1]); //output6
		
		
	
	
	
	
	
	
	}

}
