package day4;

import java.util.Arrays;
import java.util.Scanner;

public class Array2Dim {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				// jika case 1 terdapat soal 1 maka kerjakan soal 1
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {// Keterangan : done
		System.out.println(
				"===================================================================================================");

		Scanner input = new Scanner(System.in); // buat class input by scanner

		System.out.println("Masukan Panjang Array : "); // String Masukan Panjang Array
		int n = input.nextInt(); // input angka

		int nilai = 3; // nilai tetap nanti j jadi pangkat

		int[][] array = new int[3][n]; // inisialisasi array

		for (int i = 0; i < array.length; i++) { // (jika 0 < 3 maka 0 + 1) note=(untuk baris)
			if (i == 2)
				break; // jika (0 memenuhi nilai 2) perulangan berhenti
			for (int j = 0; j < array[i].length; j++) {// jika 0 < panjang dari array dimensi yang ke 2
				if (i == 1) { // jika i memenuhi 1
					System.out.print(Math.round((Math.pow(nilai, j))) + " "); // tampilkan nilai * j atau 3 pangkat dari
																				// panjang array j
				} else { // Match.pow = Method Pangkat. Math.round = method membulatkan
					System.out.print(j + " "); // tampilkan isi dari j
				}
			}
			System.out.println();
		}
	}

	private static void soal2() { // Keterangan : done
		System.out.println("Disini Soal 2");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in); // buat class input by scanner

		System.out.println("Masukan Panjang Array : "); // String Masukan Panjang Array
		int n = input.nextInt(); // input angka

		int nilai = 3; // nilai tetap nanti j jadi pangkat

		int[][] array = new int[3][n]; // inisialisasi array

		for (int i = 0; i < array.length; i++) { // (jika 0 < 3 maka 0 + 1) note=(untuk baris)
			if (i == 2)
				break; // jika (0 memenuhi nilai 2) perulangan berhenti
			for (int j = 0; j < array[i].length; j++) {// jika 0 < panjang dari array dimensi yang ke 2
				if (i == 1) { // jika i memenuhi 1
					if ((j + 1) % 3 == 0 && j != 0) { // j di mulai dari 0 jika j +1 habs di bagi 3
						System.out.print(Math.round((Math.pow(nilai, j) * -1)) + " "); // tampilkan (3 pangkat banyaknya
																						// nilai j) * -1
					} else {
						System.out.print(Math.round((Math.pow(nilai, j))) + " "); // tampilkan nilai * j atau 3 pangkat
																					// dari panjang array j
					}
				} else if ((i % 2 == 0) && (i != 0)) {
					System.out.print(Math.round((Math.pow(nilai, j))) + " ");
				} else { // Match.pow = Method Pangkat. Math.round = method membulatkan
					System.out.print(j + " "); // tampilkan isi dari j
				}
			}
			System.out.println();
		}
	}

	private static void soal3() {// Keterangan : done
		System.out.println("Disini Soal 3");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in); // buat class input by scanner

		System.out.println("Masukan Panjang Array : "); // String Masukan Panjang Array
		int n = input.nextInt(); // input angka

		int nilai = 3;

		int[][] array = new int[3][n]; // inisialisasi array

		for (int i = 0; i < array.length; i++) {
			if (i == 2)
				break;
			for (int j = 0; j < array[i].length; j++) {
				if (i == 1) {
					double middle = Math.round(Math.floor(array[i].length / 2));

					if (j >= middle) {
						System.out.print(nilai + " ");
						nilai /= 2;
					} else {
						System.out.print(nilai + " ");
						nilai *= 2;
					}

				} else {
					System.out.print(j + " ");
				}

			}
			System.out.println();
		}
	}

	private static void soal4() {// Keterangan :
		System.out.println("Disini Soal 4");
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukkan nilai N: ");
		int m = input.nextInt();
		System.out.println("Masukkan nilai N2: ");
		int n = input.nextInt();

		int i = 0;

		int[][] soalDuaDimNo4 = new int[n][m];

		System.out.println();
		System.out.println("Matriks hasilnya");
		System.out.println();

		for (int baris = 0; baris < 2; baris++) {

			if (baris % 2 == 0) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo4[baris][kolom] = i;
					System.out.print(soalDuaDimNo4[baris][kolom] + " ");
					i++;

				}
				System.out.println();
			} else if (baris % 2 == 1) {

				i = 1;
				int j = 5;
				for (int kolom = 0; kolom < m; kolom++) {

					if (kolom % 2 == 0) {
						soalDuaDimNo4[baris][kolom] = i;
						System.out.print(soalDuaDimNo4[baris][kolom] + " ");
						i++;
					} else {
						soalDuaDimNo4[baris][kolom] = j;
						System.out.print(soalDuaDimNo4[baris][kolom] + " ");
						j = j + 5;
					}

				}

				System.out.println();

			}
		}

	}

	private static void soal5() {// Keterangan :
		System.out.println("Disini Soal 5");
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukkan nilai N: ");
		int n = input.nextInt();

		int i = 0;

		int[][] soalDuaDimNo5 = new int[3][n];

		System.out.println();
		System.out.println("Matriks 3 X " + n);
		System.out.println();

		for (int baris = 0; baris < 3; baris++) {

			for (int kolom = 0; kolom < n; kolom++) {

				soalDuaDimNo5[baris][kolom] = i;
				System.out.print(soalDuaDimNo5[baris][kolom] + " ");
				i++;

			}
			System.out.println();

		}

	}

	private static void soal6() {// Keterangan :
		System.out.println("Disini Soal 6");
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukkan nilai N: ");
		int m = input.nextInt();

		int i = 0;

		int[][] soalDuaDimNo6 = new int[3][m];

		System.out.println();
		System.out.println("Matriks hasilnya");
		System.out.println();

		for (int baris = 0; baris < 3; baris++) {

			if (baris % 3 == 0) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo6[baris][kolom] = i;
					System.out.print(soalDuaDimNo6[baris][kolom] + " ");
					i++;

				}
				System.out.println();
			} else if (baris % 3 == 1) {

				i = 1;
				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo6[baris][kolom] = i;
					System.out.print(soalDuaDimNo6[baris][kolom] + " ");
					i = i * m;
				}

				System.out.println();
			} else if (baris % 3 == 2) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo6[baris][kolom] = soalDuaDimNo6[baris - 2][kolom] + soalDuaDimNo6[baris - 1][kolom];
					System.out.print(soalDuaDimNo6[baris][kolom] + " ");
					i++;

				}
			}
		}

	}

	private static void soal7() {// Keterangan :
		System.out.println("Disini Soal 7");
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukkan nilai N: ");
		int n = input.nextInt();

		int i = 0;

		int[][] soalDuaDimNo7 = new int[3][n];

		System.out.println();
		System.out.println("Matriks 3 X " + n);
		System.out.println();

		for (int baris = 0; baris < 3; baris++) {

			for (int kolom = 0; kolom < n; kolom++) {

				soalDuaDimNo7[baris][kolom] = i;
				System.out.print(soalDuaDimNo7[baris][kolom] + " ");
				i++;

			}
			System.out.println();

		}

	}

	private static void soal8() {// Keterangan :
		System.out.println("Disini Soal 8");
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukkan nilai N: ");
		int m = input.nextInt();

		int i = 0;

		int[][] soalDuaDimNo8 = new int[3][m];

		System.out.println();
		System.out.println("Matriks hasilnya");
		System.out.println();

		for (int baris = 0; baris < 3; baris++) {

			if (baris % 3 == 0) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo8[baris][kolom] = i;
					System.out.print(soalDuaDimNo8[baris][kolom] + " ");
					i++;

				}
				System.out.println();
			} else if (baris % 3 == 1) {
				i = 0;

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo8[baris][kolom] = i;
					System.out.print(soalDuaDimNo8[baris][kolom] + " ");
					i = i + 2;

				}

				System.out.println();
			} else if (baris % 3 == 2) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo8[baris][kolom] = soalDuaDimNo8[0][kolom] + soalDuaDimNo8[1][kolom];
					System.out.print(soalDuaDimNo8[baris][kolom] + " ");
					i++;

				}
			}
		}

	}

	private static void soal9() {// Keterangan :
		System.out.println("Disini Soal 9");
		System.out.println(
				"===================================================================================================");

		System.out.println("Masukkan nilai N: ");
		int m = input.nextInt();
		System.out.println("Masukkan nilai N2: ");
		int n = input.nextInt();

		int i = 0;

		int[][] soalDuaDimNo9 = new int[n][m];

		System.out.println();
		System.out.println("Matriks hasilnya");
		System.out.println();

		for (int baris = 0; baris < n; baris++) {

			if (baris % 3 == 0) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo9[baris][kolom] = i;
					System.out.print(soalDuaDimNo9[baris][kolom] + " ");
					i++;

				}
				System.out.println();
			} else if (baris % 3 == 1) {
				i = 0;

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo9[baris][kolom] = i;
					System.out.print(soalDuaDimNo9[baris][kolom] + " ");
					i = i + n;

				}
				System.out.println();
			} else if (baris % 3 == 2) {
				i = i - n;

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo9[baris][kolom] = i;
					System.out.print(soalDuaDimNo9[baris][kolom] + " ");
					i = i - n;

				}
				System.out.println();
			}
		}

	}

	private static void soal10() {// Keterangan :
		System.out.println("Disini Soal 10");
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukkan nilai N: ");
		int n = input.nextInt();

		int i = 1;

		int[] soalSepuluh = new int[n];

		System.out.println();
		System.out.println("Matriks 1 X " + n);

		for (int baris = 0; baris < n; baris++) {

			if (baris % 4 == 3) {
				i = i * 3;
				System.out.print("XXX ");
			} else {
				i = i * 3;
				soalSepuluh[baris] = i;
				System.out.print(soalSepuluh[baris] + " ");
			}

		}

	}
}
