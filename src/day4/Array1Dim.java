package day4;

import java.util.Arrays;
import java.util.Scanner;

public class Array1Dim {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				// jika case 1 terdapat soal 1 maka kerjakan soal 1
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {
		System.out.println("Disini Soal 1"); // Keterangan : done
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("masukan panjang array :");
		int p = input.nextInt();

		int[] array = new int[p];

		for (int i = 0; i < array.length; i++) {
			array[i] = i * 2 + 1;
			System.out.print(" " + (array[i])); // output 1

//			System.out.print(([i]*2+1)+" ");
		}

//		for(int i=0; i < array.length; i++) { // for untuk pengulangan  array
//		}
//		

	}

	private static void soal2() { // // Keterangan : done
		System.out.println("Disini Soal 2");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("input :");
		int n = input.nextInt();

		int[] array = new int[n];

		for (int i = 0; i < array.length; i++) {
			array[i] = (i + 1) * 2;
		}

		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
	}

	private static void soal3() { // Keterangan : done
		System.out.println("Disini Soal 3");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("input :");
		int n = input.nextInt();

		int[] array = new int[n];

		for (int i = 0; i < array.length; i++) {
			array[i] = (i * 3) + 1;
		}

		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
	}

	private static void soal4() {// Keterangan : done
		System.out.println("Disini Soal 4");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("input :");
		int n = input.nextInt();

		int[] array = new int[n];

		for (int i = 0; i < array.length; i++) {
			array[i] = (i * 4) + 1;
		}

		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
	}

	private static void soal5() {// Keterangan : done
		System.out.println("Disini Soal 5");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Panjang Array : ");
		int n = input.nextInt();

		int[] array = new int[n];

		for (int i = 0; i < array.length; i++) {
			array[i] = i;

			if (i == 5) {
				break;
			} else if (i % 2 == 0 && i != 0) {
				System.out.print("* " + ((i * 4) + 1) + " ");

			} else {
				System.out.print(((i * 4) + 1) + " ");
			}

		}
	}

	private static void soal6() {// Keterangan : done
		System.out.println("Disini Soal 6");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("input :");
		int n = input.nextInt();

		int[] array = new int[n];

		for (int i = 0; i < array.length; i++) {
			array[i] = (i * 4) + 1;

			if (array[i] % 3 == 0) { // i di masukin lg di modulusin 3 == 0
				System.out.print(" * ");
			} else {
				System.out.print(array[i] + "  ");
			}
		}

//		for (int i = 0; i < array.length; i++) {
//			
//			if(array[i]%3==0) { // i di masukin lg di modulusin 3 == 0
//				System.out.print(" * ");
//			}else {
//				System.out.print(array[i]+"  ");
//			} 
//		}
	}

	private static void soal7() {// Keterangan : done
		System.out.println("Disini Soal 7");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("input :");
		int n = input.nextInt();

		int nilai = 2;

		int[] array = new int[n];

		for (int i = 0; i < array.length; i++) {
			array[i] = i;
			System.out.print(Math.round(Math.pow(nilai, array[i] + 1)) + " ");

		}
	}

	private static void soal8() {// Keterangan : done
		System.out.println("Disini Soal 8");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Panjang Array :");
		int n = input.nextInt();

		int[] array = new int[n];
		int nilai = 3;

		for (int i = 0; i < array.length; i++) {
			array[i] = i;
			System.out.print(Math.round(Math.pow(nilai, array[i] + 1)) + " ");

		}
	}

	private static void soal9() {// Keterangan : done
		System.out.println("Disini Soal 9");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Panjang Array :");
		int n = input.nextInt();

		int[] array = new int[n];
		int nilai = 4;

		for (int i = 0; i < array.length; i++) {
			array[i] = i;

			if (i == 5) {
				break;
			} else if (i % 2 == 0 && i != 0) {
				System.out.print(" * " + Math.round(Math.pow(nilai, array[i] + 1)) + " ");
			} else {

				System.out.print(Math.round(Math.pow(nilai, array[i] + 1)) + " ");
			}

		}
	}

	private static void soal10() {// Keterangan : done
		System.out.println("Disini Soal 10");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Panjang Array :");
		int n = input.nextInt();

		int[] array = new int[n];
		int nilai = 3;

		for (int i = 0; i < array.length; i++) {
			array[i] = i;

			if (i + 1 == 4) {
				System.out.print(" XXX ");
			} else {

				System.out.print(Math.round(Math.pow(nilai, array[i] + 1)) + " ");
			}

		}
	}
}
