package arsip;

import java.util.Arrays;
import java.util.Scanner;

public class templateFieldSoal {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				// jika case 1 terdapat soal 1 maka kerjakan soal 1
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {// Keterangan :
		System.out.println(
				"===================================================================================================");

	}

	private static void soal2() { // Keterangan :
		System.out.println("Disini Soal 2");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal3() {// Keterangan :
		System.out.println("Disini Soal 3");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal4() {// Keterangan :
		System.out.println("Disini Soal 4");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal5() {// Keterangan :
		System.out.println("Disini Soal 5");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal6() {// Keterangan :
		System.out.println("Disini Soal 6");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal7() {// Keterangan :
		System.out.println("Disini Soal 7");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal8() {// Keterangan :
		System.out.println("Disini Soal 8");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal9() {// Keterangan :
		System.out.println("Disini Soal 9");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal10() {// Keterangan :
		System.out.println("Disini Soal 10");
		System.out.println(
				"===================================================================================================");

	}
}
