package day7;

import java.util.Scanner;
//done
public class warmUpSoal7 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Array : ");
		String huruf = input.nextLine();

		String[] arrayHuruf = huruf.split(" "); // inisialisasi array integer menampung array string
		int nilaiMax = -9999999;
		int nilaiMin = 99999999;

		for (int i = 0; i < arrayHuruf.length; i++) {
			int sum = 0;

			for (int j = 0; j < arrayHuruf.length; j++) {
				if (i != j) {
					sum += Integer.parseInt(arrayHuruf[j]);
				}
			}
			
			
			if (nilaiMax < sum) {
				nilaiMax = sum;
			}
			
			if (nilaiMin > sum) {
				nilaiMin = sum;
			}

		}

		System.out.println(nilaiMax + " " + nilaiMin);
		

	}

}
