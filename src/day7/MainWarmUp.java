package day7;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Scanner;

public class MainWarmUp {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				// jika case 1 terdapat soal 1 maka kerjakan soal 1
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				case 11:
					soal1();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {// Keterangan :
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukan Angka a: ");
		int a = input.nextInt();
		System.out.println("Masukan Angka b: ");
		int b = input.nextInt();
		System.out.println(a + b);
	}

	private static void soal2() { // Keterangan :
		System.out.println("Disini Soal 2");
		System.out.println(
				"===================================================================================================");
		System.out.println("*Soal 4 - Time Conversion*");
		System.out.println();

		System.out.print("Masukkan Waktu yang akan dikonversi (HH:MM:SS[AM/PM]): ");
		String inputWaktu = input.next();

		if (inputWaktu.length() != 10) {
			System.out.println("Maaf, waktu yang anda masukkan salah karena tidak memenuhi kaidah AM/PM");
			return;
		}

		String waktu = inputWaktu.substring(0, 8);
		String[] waktuStr = waktu.split(":");
		int[] waktuInt = new int[waktuStr.length];
		String hasil = "";
		String ampm = inputWaktu.substring(8, 10).toLowerCase();

		for (int i = 0; i < waktuStr.length; i++) {
			waktuInt[i] = Integer.parseInt(waktuStr[i]);
		}

		if (waktuInt[0] > 0 && waktuInt[0] <= 12) {

			if (ampm.equals("pm")) {
				if (waktuInt[0] < 12 && waktuInt[0] > 0) {
					waktuInt[0] += 12;
					waktuStr[0] = "" + waktuInt[0];
				}
			} else if (ampm.equals("am")) {
				if (waktuInt[0] == 12) {
					waktuStr[0] = "00";
				}
			}
			hasil = waktuStr[0] + ":" + waktuStr[1] + ":" + waktuStr[2];

		} else {
			System.out.println("Maaf, waktu yang anda masukkan salah karena tidak memenuhi kaidah AM/PM");
		}

		System.out.println(hasil);

	}

	private static void soal3() {// Keterangan :
		System.out.println("Disini Soal 3");
		System.out.println(
				"===================================================================================================");
		System.out.println("Simple Array Sum");
		System.out.println("Masukan Panjang Array : ");
		int panjangArray = input.nextInt(); // input panjang array

		int[] array = new int[panjangArray]; // inisialisasi array
		input.nextLine(); // next integer berubah menjadi string perubahan
		System.out.println("Masukan nilai Array : ");
		String angka1 = input.nextLine(); // input nilai array

		String[] angka2 = angka1.split(" ");
		int[] arrayInputConvert = new int[angka2.length];

		int jumlah = 0;

		for (int i = 0; i < array.length; i++) {
			arrayInputConvert[i] = Integer.parseInt(angka2[i]);
			jumlah = jumlah + arrayInputConvert[i];
//			System.out.println(jumlah);
			if (i == array.length - 1) {

				System.out.print(jumlah);
			}

		}
		System.out.println();
	}

	private static void soal4() {// Keterangan :
		System.out.println("Disini Soal 4");
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukan Panjang Array : ");
		int panjangArray = input.nextInt(); // input angka
		input.nextLine();
		int[][] array = new int[panjangArray][panjangArray]; // inisialisasi panjang array

		int inputArray = 0; // input [3][3]

		int diagonal1 = 0;
		int diagonal2 = 0;

		for (int i = 0; i < panjangArray; i++) { // 0 < 3 makanu

			System.out.println("input baris array ke : " + (i + 1));
			String baris = input.next();// input baris
			String[] inputBaris = baris.split(",");
			for (int j = 0; j < panjangArray; j++) {
				array[i][j] = Integer.parseInt(inputBaris[j]);
			}

			System.out.println();
		}

		for (int i = 0; i < array.length; i++) {
			diagonal1 += array[i][i];
			diagonal2 += array[i][array.length - 1 - i];
		}
		System.out.println(Math.abs(diagonal1 - diagonal2));
	}

	private static void soal5() {// Keterangan :
		System.out.println("Disini Soal 5");
		System.out.println(
				"===================================================================================================");
		System.out.println("Diagonal Difference");

		System.out.println("Masukan Array : ");
		String huruf = input.nextLine();

		String[] arrayHuruf = huruf.split(" "); // inisialisasi array integer menampung array string
		int nilaiMax = -9999999;
		int nilaiMin = 99999999;

		for (int i = 0; i < arrayHuruf.length; i++) {
			int sum = 0;

			for (int j = 0; j < arrayHuruf.length; j++) {
				if (i != j) {
					sum += Integer.parseInt(arrayHuruf[j]);
				}
			}

			if (nilaiMax < sum) {
				nilaiMax = sum;
			}

			if (nilaiMin > sum) {
				nilaiMin = sum;
			}

		}

		System.out.println(nilaiMax + " " + nilaiMin);
	}

	private static void soal6() {// Keterangan :
		System.out.println("Disini Soal 6");
		System.out.println(
				"===================================================================================================");
		System.out.println("Plus Minus");

		System.out.println("Panjang Array : ");
		int arrayLength = input.nextInt();// panjang array
		input.nextLine();
		System.out.println("Masukan Nilai  : ");
		String value = input.nextLine();

		String[] arrayValue = value.split(" ");

		int negative = 0;
		int zero = 0;
		int positive = 0;

		for (int i = 0; i < arrayValue.length; i++) {

			int nilai = Integer.parseInt(arrayValue[i]);
			if (nilai < 0) {
				negative++;
			} else if (nilai == 0) {
				zero++;
			} else {
				positive++;
			}
		}

		// reformat output menjadi 6 angka dibelakang koma
		DecimalFormat decimalFormat = new DecimalFormat("0.000000");
		value = decimalFormat.format((float) (positive) / arrayLength) + "\n"
				+ decimalFormat.format((float) (negative) / arrayLength) + "\n"
				+ decimalFormat.format((float) (zero) / arrayLength);

		// output
		System.out.println(value);

	}

	private static void soal7() {// Keterangan :
		System.out.println("Disini Soal 7");
		System.out.println(
				"===================================================================================================");
		System.out.println("Staircase");
		System.out.println("Masukkan n: ");
		int n = input.nextInt();

		// pola segitiga siku-siku
		String temp = "";
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i + j >= n - 1) {
					temp += "#";
				} else {
					temp += " ";
				}
			}
			temp += "\n";
		}

		// output
		System.out.println(temp);

	}

	private static void soal8() {// Keterangan :
		System.out.println("Disini Soal 8");
		System.out.println(
				"===================================================================================================");
		System.out.println("Mini Max sum");

		System.out.println("Masukan Array : ");
		String huruf = input.nextLine();

		String[] arrayHuruf = huruf.split(" "); // inisialisasi array integer menampung array string
		int nilaiMax = -9999999;
		int nilaiMin = 99999999;

		for (int i = 0; i < arrayHuruf.length; i++) {
			int sum = 0;

			for (int j = 0; j < arrayHuruf.length; j++) {
				if (i != j) {
					sum += Integer.parseInt(arrayHuruf[j]);
				}
			}

			if (nilaiMax < sum) {
				nilaiMax = sum;
			}

			if (nilaiMin > sum) {
				nilaiMin = sum;
			}

		}

		System.out.println(nilaiMax + " " + nilaiMin);
	}

	private static void soal9() {// Keterangan :
		System.out.println("Disini Soal 9");
		System.out.println(
				"===================================================================================================");
		System.out.println("Birthday Candle Cakes");
		System.out.println("Panjang Array : ");
		int arrayLength = input.nextInt();// panjang array

		input.nextLine();

		System.out.println("Masukan Nilai  : ");
		String value = input.nextLine();

		String[] arrayValue = value.split(" ");

		int[] integerArrayValue = new int[arrayValue.length];

		int nilaiTerbesar = 0;
		for (int i = 0; i < arrayValue.length; i++) {
			integerArrayValue[i] = Integer.parseInt(arrayValue[i]); // 3 1 2 3
			if (integerArrayValue[i] > 2) {
				nilaiTerbesar += 1; // isi array + isi array
			}
		}

		System.out.println(nilaiTerbesar);

	}

	private static void soal10() {// Keterangan :
		System.out.println("Disini Soal 10");
		System.out.println(
				"===================================================================================================");
		System.out.println("A Very Big Sum");
		System.out.println("Masukkan Panjang Array : ");
		int array = input.nextInt();

		input.nextLine();

		System.out.println("Masukan nilai : ");
		String inputLong = input.nextLine();

		String[] stringLength = inputLong.split(" ");

		int[] intArray = new int[stringLength.length]; // panjang array

//penjumlahan bigDecimal
		BigDecimal sementara = new BigDecimal(0);
		for (int i = 0; i < intArray.length; i++) { // 0 < 5
			intArray[i] = Integer.parseInt(stringLength[i]); //
			sementara = sementara.add(new BigDecimal(intArray[i]));
		}
		System.out.println(sementara);
	}

	private static void soal11() {// Keterangan :
		System.out.println("Disini Soal 11");
		System.out.println(
				"===================================================================================================");
		System.out.println("Compare the Triples");
		System.out.println("Masukkan array (2x3): ");
		String string1 = input.nextLine();
		input.nextLine();
		String string2 = input.nextLine();

		String[] arrayString1 = string1.split(" ");
		String[] arrayString2 = string2.split(" ");

// pengaman jika panjang array tidak sama
		if (arrayString1.length != arrayString2.length) {
			System.out.println("Panjang array tidak sama, mohon diinput ulang!");
			return;
		}

		int pointAlice = 0;
		int pointBob = 0;

		for (int i = 0; i < arrayString1.length; i++) {
			int alice = 0;
			for (int j = 0; j < arrayString2.length; j++) {
				int bob = 0;

				if (i == j) {
					alice = Integer.parseInt(arrayString1[i]);
					bob = Integer.parseInt(arrayString2[j]);
					if (alice > bob) {
						pointAlice++;
					} else if (alice < bob) {
						pointBob++;
					}
				}

			}
		}

		System.out.println(pointAlice + " " + pointBob);

	}
}
