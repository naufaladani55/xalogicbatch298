package day7;

import java.util.Scanner;
//done
public class tesSoal4 {

	public static void main(String[] args) {

		int[][] angka = { { 11, 2, 4 }, { 4, 5, 6 }, { 10, 8, -12 } }; // inisialisasi array

		int diagonal1 = 0;
		int diagonal2 = 0;
		for (int i = 0; i < angka.length; i++) { // buat nampilin isi array
			diagonal1 += angka[i][i];
			diagonal2 += angka[i][angka.length - 1 - i];
		}
		System.out.println(Math.abs(diagonal1 - diagonal2));
	}

}
