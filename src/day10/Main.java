package day10;

//soal sort
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				// jika case 1 terdapat soal 1 maka kerjakan soal 1
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {// Keterangan : donel.
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Banyak Angka");
		int pAngka = input.nextInt();
		String[] panjangAngka = new String[pAngka];
//		int[] penampung = new int[panjangAngka.length];
//		String isi = "";

		for (int i = 0; i < pAngka; i++) {
			System.out.println();
			System.out.println("Masukan Angka ke : " + (i + 1));
			panjangAngka[i] = input.next();
//		
			System.out.print(panjangAngka[i] + " ");
		}
		System.out.println();

//		for (int i = 0; i < panjangAngka.length; i++) {
//			
//		}

		// convert string array ke int array
		BigDecimal[] panjangAngkaInt = new BigDecimal[panjangAngka.length];
		for (int i = 0; i < panjangAngkaInt.length; i++) {
			panjangAngkaInt[i] = new BigDecimal(panjangAngka[i]);
		}
		// sorting
		BigDecimal penampung = new BigDecimal(0);

		for (int i = 0; i < panjangAngkaInt.length; i++) {
			for (int j = 0; j < panjangAngkaInt.length; j++) {
				int pembanding = 0;
				pembanding = panjangAngkaInt[i].compareTo(panjangAngkaInt[j]); // pembanding BigDecimal

				if (pembanding == -1) {
					penampung = panjangAngkaInt[i]; // penampung di masukan isi dari Array Int I
					panjangAngkaInt[i] = panjangAngkaInt[j]; // array int [i] di masukan panjangAngka.int
					panjangAngkaInt[j] = penampung; // nilai J di tampung penampung
				}
			}
		}

		// tampil
		for (int i = 0; i < panjangAngkaInt.length; i++) {
			System.out.print(panjangAngkaInt[i] + " ");

		}

	}

	private static void soal2() { // Keterangan :
		System.out.println("Disini Soal 2");
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Panjang Array : ");
		int pAngka = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Angka : ");
		String angka = input.nextLine();
		String[] panjangAngka = angka.split(" ");
		int[] panjangAngkaInt = new int[panjangAngka.length];

		for (int i = 0; i < panjangAngka.length; i++) {

			panjangAngkaInt[i] = Integer.parseInt(panjangAngka[i]);
		}

		for (int i = 0; i < panjangAngkaInt.length; i++) {
			for (int j = 0; j < panjangAngkaInt.length; j++) {
				int penampung = 0;

				System.out.println(penampung);
				
			}
		}
	}

	private static void soal3() {// Keterangan :
		System.out.println("Disini Soal 3");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal4() {// Keterangan :
		System.out.println("Disini Soal 4");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal5() {// Keterangan :
		System.out.println("Disini Soal 5");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal6() {// Keterangan :
		System.out.println("Disini Soal 6");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal7() {// Keterangan :
		System.out.println("Disini Soal 7");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal8() {// Keterangan :
		System.out.println("Disini Soal 8");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal9() {// Keterangan :
		System.out.println("Disini Soal 9");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal10() {// Keterangan :
		System.out.println("Disini Soal 10");
		System.out.println(
				"===================================================================================================");

	}
}
