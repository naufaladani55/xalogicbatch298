package Kisiday11;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				// jika case 1 terdapat soal 1 maka kerjakan soal 1
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	@SuppressWarnings("deprecation")
	private static void soal1() throws ParseException {// Keterangan :
		System.out.println(
				"===================================================================================================");
		System.out.println("masukan Hari Masuk Mery");
		int mery = input.nextInt();
		System.out.println("masukan Hari Masuk Susan");
		int susan = input.nextInt();
//		int persamaan = 0;

		input.nextLine();
		System.out.println("Masukan Tgl Ketemu z");
		String tglZ = input.nextLine().toLowerCase();
		String[] tgl = tglZ.split(" ");
		String nol = "";
		SimpleDateFormat date = new SimpleDateFormat("dd MMMM yyyy");
		Date kz = date.parse(tglZ);

		int salah = 0;

		int tanggalInt = Integer.parseInt(tgl[0]);
		int bulan = kz.getMonth();
		int tahun = Integer.parseInt(tgl[2]);

		if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10 || bulan == 12) {
			if (tanggalInt < 0 || tanggalInt > 31) {
				salah++;
			} else if (tahun > 2022) {
				salah++;
			}
		} else if (bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11) {
			if (tanggalInt < 0 || tanggalInt > 30) {
				salah++;
			}
		} else if (tahun % 4 == 0 && bulan == 2) {
			if (tanggalInt < 0 || tanggalInt > 29) {
				salah++;
			}
		} else if (tahun % 4 != 0 && bulan == 2) {
			if (tanggalInt < 0 || tanggalInt > 28) {
				salah++;
			}
		}

		if (salah > 0) {
			System.out.println("Maaf, inputan anda salah.");
			return;
		}

		// cari kpk
		int kpk = 0;
		if (mery < susan) {
			kpk = mery;
		} else if (mery > susan) {
			kpk = susan;
		}

		int hitung = 0;
		boolean nilai = false;
		while (nilai = true) {
			hitung += kpk;
			if (hitung % mery == 0 && hitung % susan == 0) {
				break;
			} else {
				nilai = true;
			}
		}

//		// cari kpk
//		for (int i = 0; i < susan + 1; i++) {
//			persamaan = (persamaan + mery + 1); // 0+(3+1)
//			if (persamaan % susan == 0) {
//				// System.out.println("kpk" + persamaan);
//			} else {
//				System.out.println("Ga sesuai");
//				break;
//			}
//
//		}
		long ketemu = hitung;// 12
		long hariSama = ketemu * (24 * 60 * 60 * 1000);
//		System.out.println(ketemu);
		long timestamp1 = kz.getTime();
		long resultlong = timestamp1 + hariSama;
		Date resultDate = new Date(resultlong);
		nol = nol + (String) date.format(resultDate);
		System.out.println(nol);

	}

	// baris 102
//	System.out.println("Masukan Sambungan tanggal  = MM-yyyy");
//	String sambunganTgl = input.next();

	// convert int to string
//	String persamaanTGL = String.valueOf(persamaan);//12
//	String tglTambah = persamaanTGL+"-"+sambunganTgl;

//	System.out.println(persamaanTGL+"-"+sambunganTgl);// 6,

//	Date tambahanTgl= date.parse(tglTambah);
//    System.out.println(tambahanTgl);   
//	
//	long timestamp2 = tambahanTgl.getTime();
	private static void soal2() { // Keterangan :
		System.out.println("Disini Soal 2");
		System.out.println(
				"===================================================================================================");
		System.out.println("Masukkan kalimat: ");
		char[] inputChar = input.nextLine().toLowerCase().trim().toCharArray();
		String output = "";

		// sorting
		char tempChar = 0;
		for (int i = 0; i < inputChar.length; i++) {
			for (int j = 0; j < inputChar.length; j++) {
				if (inputChar[i] < inputChar[j]) {
					tempChar = inputChar[i];
					inputChar[i] = inputChar[j];
					inputChar[j] = tempChar;
				}
			}
		}
		System.out.println(inputChar);

		tempChar = 0;
		for (int i = 0; i < inputChar.length; i++) {
			if (inputChar[i] != 32) {
				if (tempChar != inputChar[i] && i > 1) {
					output += " - ";
				}
				tempChar = inputChar[i];
				output += inputChar[i];
			}

		}

		System.out.println(output);

	}

	private static void soal3() {// Keterangan :
		System.out.println("Disini Soal 3");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal4() {// Keterangan :
		System.out.println("Disini Soal 4");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal5() {// Keterangan :
		System.out.println("Disini Soal 5");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal6() {// Keterangan :
		System.out.println("Disini Soal 6");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal7() {// Keterangan :
		System.out.println("Disini Soal 7");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal8() {// Keterangan :
		System.out.println("Disini Soal 8");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal9() {// Keterangan :
		System.out.println("Disini Soal 9");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal10() {// Keterangan :
		System.out.println("Disini Soal 10");
		System.out.println(
				"===================================================================================================");

	}
}
