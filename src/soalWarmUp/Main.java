package soalWarmUp;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				// jika case 1 terdapat soal 1 maka kerjakan soal 1
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {
		System.out.println("Disini Soal 1"); // done
		System.out.println(
				"===================================================================================================");

		System.out.println("Masukan Panjang Array : ");
		int panjangArray = input.nextInt(); // input panjang array

		int[] array = new int[panjangArray]; // inisialisasi array
		input.nextLine(); // next integer berubah menjadi string perubahan
		System.out.println("Masukan nilai Array : ");
		String angka1 = input.nextLine(); // input nilai array

		String[] angka2 = angka1.split(" ");
		int[] arrayInputConvert = new int[angka2.length];

		int jumlah = 0;

		for (int i = 0; i < array.length; i++) {
			arrayInputConvert[i] = Integer.parseInt(angka2[i]);
			jumlah = jumlah + arrayInputConvert[i];
//			System.out.println(jumlah);
			if (i == array.length - 1) {

				System.out.print(jumlah);
			}

		}
		System.out.println();
//			}
	}

	private static void soal2() {
		System.out.println("Disini Soal 2"); // done
		System.out.println(
				"===================================================================================================");

		Scanner input = new Scanner(System.in); // class fungsi input

		System.out.println("Masukan Panjang Array : ");
		int panjangArray = input.nextInt(); // input angka
		input.nextLine();
		int[][] array = new int[panjangArray][panjangArray]; // inisialisasi panjang array

		int inputArray = 0; // input [3][3]

		int diagonal1 = 0;
		int diagonal2 = 0;

		for (int i = 0; i < panjangArray; i++) { // 0 < 3 makanu

			System.out.println("input baris array ke : " + (i + 1));
			String baris = input.next();// input baris
			String[] inputBaris = baris.split(",");
			for (int j = 0; j < panjangArray; j++) {
				array[i][j] = Integer.parseInt(inputBaris[j]);
			}

			System.out.println();
		}

		for (int i = 0; i < array.length; i++) {
			diagonal1 += array[i][i];
			diagonal2 += array[i][array.length - 1 - i];
		}
		System.out.println(Math.abs(diagonal1 - diagonal2));
	}

	private static void soal3() {
		System.out.println("Disini Soal 3"); // done
		System.out.println(
				"===================================================================================================");

		Scanner input = new Scanner(System.in);

		System.out.println("Input Panjang Array : ");
		int arrayPanjang = input.nextInt(); // 6

		input.nextLine();

		System.out.println("Masukan Isi Array");
		String IisiArray = input.nextLine(); // -4 3 -9 0 4 1

		String[] arrayIsi = IisiArray.split(" "); // proses string ke dalam array string

		int[] arrayInt = new int[arrayIsi.length]; // panjang array di ambil dari panjang berdasarkan array string

		double nilai1 = 0;
		double nilai2 = 0;
		double nilai3 = 0;

		for (int i = 0; i < arrayInt.length; i++) {
			arrayInt[i] = Integer.parseInt(arrayIsi[i]);

			if (arrayInt[i] > 0) {
				nilai1++;
			}
			if (arrayInt[i] < 0) {
				nilai2++;
			}
			if (arrayInt[i] == 0) {
				nilai3++;
			}
		}
		System.out.println(Math.round((nilai1 / arrayInt.length) * 100000d) / 100000d);
		System.out.println(nilai2 / arrayInt.length);
		System.out.println(nilai3 / arrayInt.length);

	}

	private static void soal4() {
		System.out.println("Disini Soal 4"); // belum selesai
		System.out.println(
				"===================================================================================================");
		// inisialisasi panjang array
		
		System.out.println("Simple Array Sum"); 

		Scanner input = new Scanner(System.in);
		System.out.println("Panjang Array : ");
		int arrayLength = input.nextInt();

		int[] inputArray = new int[arrayLength];

		for (int i = 0; i < inputArray.length; i++) {
			for (int j = 0; j < inputArray[i]; j++) {
//				System.out.println(inputArray[i][j] + "");
			}
		}
		System.out.println();
	}

	private static void soal5() { // done
		System.out.println("Disini Soal 5");// done
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Array : ");
		String huruf = input.nextLine();

		String[] arrayHuruf = huruf.split(" "); // inisialisasi array integer menampung array string
		int nilaiMax = -9999999;
		int nilaiMin = 99999999;

		for (int i = 0; i < arrayHuruf.length; i++) {
			int sum = 0;

			for (int j = 0; j < arrayHuruf.length; j++) {
				if (i != j) {
					sum += Integer.parseInt(arrayHuruf[j]);
				}
			}

			if (nilaiMax < sum) {
				nilaiMax = sum;
			}

			if (nilaiMin > sum) {
				nilaiMin = sum;
			}

		}

		System.out.println(nilaiMax + " " + nilaiMin);

	}

	private static void soal6() {
		System.out.println("Disini Soal 6"); // done
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("Panjang Array : ");
		int arrayLength = input.nextInt();// panjang array
		input.nextLine();
		System.out.println("Masukan Nilai  : ");
		String value = input.nextLine();

		String[] arrayValue = value.split(" ");


		int negative = 0;
		int zero = 0;
		int positive = 0;

		
		for (int i = 0; i < arrayValue.length; i++) {
		
			int nilai =Integer.parseInt(arrayValue[i]);
			if(nilai<0) {
				negative++;
			}else if (nilai ==0) {
				zero++;
			}else {
				positive++;
			}
		}
		
		
		// reformat output menjadi 6 angka dibelakang koma
				DecimalFormat decimalFormat = new DecimalFormat("0.000000");
				value = decimalFormat.format((float) (positive) / arrayLength) + "\n" + decimalFormat.format((float) (negative) / arrayLength)
						+ "\n" + decimalFormat.format((float) (zero) / arrayLength);

				// output
				System.out.println(value);

	}

	private static void soal7() {
		System.out.println("Disini Soal 7"); // done
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan Panjang Array : ");
		int array = input.nextInt();

		input.nextLine();

		System.out.println("Masukan nilai : ");
		String inputLong = input.nextLine();

		String[] stringLength = inputLong.split(" ");

		int[] intArray = new int[stringLength.length]; // panjang array

		// penjumlahan bigDecimal
		BigDecimal sementara = new BigDecimal(0);
		for (int i = 0; i < intArray.length; i++) { // 0 < 5
			intArray[i] = Integer.parseInt(stringLength[i]); //
			sementara = sementara.add(new BigDecimal(intArray[i]));
		}
		System.out.println(sementara);
	}

	private static void soal8() { // belum
		System.out.println("Disini Soal 8");
		System.out.println(
				"===================================================================================================");

	}

}
