package day9;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				// jika case 1 terdapat soal 1 maka kerjakan soal 1
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {// Keterangan :
		System.out.println(
				"===================================================================================================");
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Point Anda : ");
		int point = input.nextInt(); // variabel point

		boolean statusPermainan = true;

		do { // ketika udah masukin point maka di sini lakukan terus penginputan
				// perhitungan logic Random
			int generate = ((int) (Math.random() * (9 - 0)) + 0);
			String result = "";

			System.out.println("Masukan Taruhan Anda : ");
			int bet = input.nextInt(); // variabel taruhan

			// validasi Taruhan Lebih besar Dari Pada Point
			if (bet > point) {
				System.out.println(
						"Point Anda Kurang! " + "Point Anda Adalah : " + point + " Silahkan Ganti Taruhan Anda!");
				System.out.println();
				continue;
			}

			System.out.println("Masukan Tebakan Anda : ");
			String guess = input.next(); // variabel tebakan1

			// validasi TEBAKAN
			if ((guess.equals("D")) && (generate < 5)) { // kondisi D < 5 (true) You Win dan Point + jumlah taruhan
				result = "YOU WIN!";
				point += bet;
			}

			if ((guess.equals("D")) && (generate > 5)) {
				result = "YOU LOSE!";
				point -= bet;
			}

			if ((guess.equals("U")) && (generate > 5)) {
				result = "YOU WIN!";
				point += bet;
			}

			if ((guess.equals("U")) && (generate < 5)) {
				result = "YOU LOSE!";
				point -= bet;
			}

			if (guess.equals("D") && generate == 5 || guess.equals("D") && generate == 5) {
				result = "DRAW!";
			}

			// VALIDASI JIKA POINT < 0 MAKA GAME BERAKHIR
			if (point <= 0) {
				System.out.println("GAME OVER!");
				break; // SINTAX DIBAWAH TIDAK DI ULANG
			}
			System.out.println();
			System.out.println("TEBAKAN YANG BENAR = " + generate);
			System.out.println(result);
			System.out.println("POINT SAAT INI = " + point);
			System.out.println("Mau Lanjut ga Bestie? : Y/N ");

			String lanjut = input.next();
			// validasi game lanjut atau ngga
			if (lanjut.equals("N")) {
				statusPermainan = false;
			}

		} while (statusPermainan); // permainan

	}

	private static void soal2() { // Keterangan :
		System.out.println("Disini Soal 2");
		System.out.println(
				"===================================================================================================");
		System.out.println("Isi Keranjang 1");
		int keranjang1 = input.nextInt();
		System.out.println("Isi Keranjang 2");
		int keranjang2 = input.nextInt();
		System.out.println("Isi Keranjang 3");
		int keranjang3 = input.nextInt();
		System.out.println("Keranjang Yang dibawa ke Pasar");
		int diBawa = input.nextInt();

		int[] keranjang = new int[3];
		int[] keranjangSisa = new int[2];

		keranjang[0] = keranjang1;
		keranjang[1] = keranjang2;
		keranjang[2] = keranjang3;

		int nilai1 = 0;
		int nilai2 = 0;

		for (int i = 0; i < keranjang.length; i++) {
			if (diBawa - 1 == i) {
				continue;
			}
			keranjangSisa[i] = keranjang[i];
		}

		for (int i = 0; i < keranjangSisa.length; i++) {
			nilai1 += keranjangSisa[i];
			nilai2 = nilai1;
		}
		System.out.println("Sisa Buahnya Adalah : " + nilai1);
	}

	private static void soal3() {// Keterangan :
		System.out.println("Disini Soal 3");
		System.out.println(
				"===================================================================================================");
		System.out.println("Banyak Anak : ");
		int anak = input.nextInt(); // 3
		int[] banyakAnak = new int[anak]; // 3

		int nilai1 = 1; // nilai penampung
		int nilai2 = 0;

		for (int i = 0; i < anak; i++) { // 0 1 2
			banyakAnak[i] = i + 1; // 1 // 1 , 2 , 3 +1
		}
		// 3! = 3 x 2 x 1
		for (int i = 0; i < banyakAnak.length; i++) {
			nilai1 *= banyakAnak[i];
			nilai2 = nilai1;

		}
		System.out.println("ada " + nilai1 + " cara");

	}

	private static void soal4() {// Keterangan :
		System.out.println("Disini Soal 4");
		System.out.println(
				"===================================================================================================");
		System.out.println();

		System.out.println("Masukan Jumlah Keranjang");
		int keranjang = input.nextInt();
		String buah[] = new String[keranjang];
		System.out.println("Masukan Jumlah Keranjang Yang Dibawa");
		int diBawa = input.nextInt();
	}

	private static void soal5() {// Keterangan :
		System.out.println("Disini Soal 5");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal6() throws ParseException {// Keterangan :
		System.out.println("Disini Soal 6");
		System.out.println(
				"===================================================================================================");
//		long diffSeconds = diff / 1000;
//		long diffMinutes = diff / (60 * 1000);
//		long diffHours = diff / (60 * 60 * 1000);
//		long diffDays = diff / (24 * 60 * 60 * 1000);
//		long diffMonths = (diff / (24 * 60 * 60 * 1000)) / 30;
//		long diffYears = ((diff / (24 * 60 * 60 * 1000)) / 30) / 12;
		
		SimpleDateFormat date1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date tanggalMasuk = date1.parse("20-08-2019 07:00");
		Date tanggalKeluar = date1.parse("20-08-2019 16:00");
		long result = 0;
		long timestamp1 = tanggalMasuk.getTime();
		long timestamp2 = tanggalKeluar.getTime();
		result = Math.abs(timestamp2 - timestamp1);
		System.out.println();
		long jam = ((result / (60 * 60 * 1000)));
		long menit = ((result / (60 * 1000)));
		float koma = menit / 60f;
		long koma1 = menit / 60;

		float hitungMenit = (koma - koma1) * 60;
		System.out.println((long) hitungMenit);
		if ((long) hitungMenit > 30) {
			koma1 += 1;
		}
		if (koma1 > 5) {
			koma1 = 5;
		}
		System.out.println(koma1);
		System.out.println(koma1 * 3000);
//		SimpleDateFormat date2 = new SimpleDateFormat("20/08/2019 17.30");
//		System.out.println(date1.format(new Date()));
//		System.out.println(date2.format(new Date()));
//		Date timestamp = date1.getTime();

//		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		Date date = format.parse("your string goes here");
//		long timestamp = date.getTime();
	}

	private static void soal7() throws ParseException {// Keterangan :
		System.out.println("Disini Soal 7");
		System.out.println(
				"===================================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
		System.out.println("Input Tanggal Pinjam dengan Format dd-MM-yyyy");
		String pinjam = input.next();
		System.out.println("Input Tanggal Pengembalian dengan Format dd-MM-yyyy");
		String kembali = input.next();
		Date pinjamz = date.parse(pinjam);
		Date kembaliz = date.parse(kembali);
		System.out.println(pinjamz);
		System.out.println(kembaliz);
		// ngubah waktu menjadi timestamp
		long timestamp1 = pinjamz.getTime();
		long timestamp2 = kembaliz.getTime();
		long result = timestamp2 - timestamp1;
		System.out.println();
		long hari = result / (24 * 60 * 60 * 1000);
		long denda = 500;
		long bayarDenda = 0;
		if(hari > 3) {
			bayarDenda =  (hari -3)*denda;
		}
		System.out.println("Rp "+bayarDenda);
		System.out.println(hari);
	}

	private static void soal8() {// Keterangan :
		System.out.println("Disini Soal 8");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal9() {// Keterangan :
		System.out.println("Disini Soal 9");
		System.out.println(
				"===================================================================================================");

	}

	private static void soal10() {// Keterangan :
		System.out.println("Disini Soal 10");
		System.out.println(
				"===================================================================================================");

	}
}
