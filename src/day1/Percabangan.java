package day1;

import java.util.Scanner; //caranya ketik Scanner trus teken ctrl +  space

public class Percabangan {
	public static void main(String[] args) {
		
		
//		System.out.println("Selamat Pagi");
//		System.out.println("Selamat Datang");
//	
//		boolean mauKeluar = true;
//		
//		if (mauKeluar) {
//			System.out.println("Sampai Jumpa");
//		}else {
//			System.out.println("Tidak jadi keluar");
//		}
//		
//		CASE 1
		
		//Batch 289 lulus jika memiliki nilai lebih besar
		//atau sama dengan 75
		Scanner input = new Scanner(System.in);//input = variabel 
		
		
		System.out.println("Masukan Nama Siswa: ");
		String name= input.next(); // untuk menginput kan nama (String)
		
		
		System.out.println("Masukan Nilai Saya: ");
		int nilaiSaya = input.nextInt(); //caranya ketik input integer.
		
        System.out.println("Masukan Nilai Softskill saya: "); // untuk menginput nilai int
		int nilaiSoftSkill = input.nextInt();
		
		
		input.nextLine();// nextLine ini agar penulisan Suatu Tipe data misal string tidak ke skip
		System.out.println("Masukan Nomor Batch");
		String batch = input.nextLine(); // untuk menginout string yang bisa menggunakan spasi
		
		
		
		System.out.println("Masukan Nomor Ruangan");
		int room = input.nextInt();
		
		String grade = "";
		String kelulusan = "";
		
		if (nilaiSaya >= 90  && nilaiSoftSkill >= 3 ) {
			//statement
			grade = "A"; 
		    kelulusan = "Lulus";
		  
		}else if (nilaiSaya >= 85  && nilaiSoftSkill >= 3) {
			grade = "B+";
			kelulusan = "Lulus";
		}else if (nilaiSaya >= 80 && nilaiSoftSkill >= 3) {
			grade = "B";
			kelulusan = "Lulus";
		}else if (nilaiSaya >= 75 && nilaiSoftSkill >=3) {
			grade = "C+";
			kelulusan ="Lulus";
		}else if(nilaiSaya >= 70 && nilaiSoftSkill >=3) {
			grade = "C";
			kelulusan= "Remedial";
		}else {
			grade = "D";
			kelulusan = "tidak lulus";
		}
		System.out.println(name+" " + batch +" Dengan Grade "+ grade+" " +kelulusan +" Ruangan " + room);
	}
}
