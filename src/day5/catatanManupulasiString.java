package day5;

public class catatanManupulasiString {

	public static void main(String[] args) {
		//String manipulation
		
		//memisahkan kalimat menjadi perkata (menggunakan split)
		String words1 = "Andre Muhammad Satrio"; // setiap ada spasi di ambil per kata 
		String[] tempWords1 = words1.split(" "); // setiap spasi itu artinya koma (,) pemisah
						
		
		System.out.println(tempWords1[2]);						//mau menampilkan satrio memanggil index ke 2;
		System.out.println(tempWords1[tempWords1.length - 1]); // panjang array tempWords -1 hasilnya satrio
		
		
		//mengubah kata menjadi karakter (menggunakan toCharArray) 
		char[] charWords1 = tempWords1[2].toCharArray(); //inisialisasi untuk memecahkan dari kata per huruf menggunakan sintax toCharArray();
		System.out.println(charWords1[charWords1.length-1]); // menamppilkan kata dari panjang array dan ( index = panjang array-1)

		//replace mengganti character dari kalimat
		String replaceWords1 = words1.replaceAll(" ",""); // mengganti kata spasi menjadi tidak ada spasi
		System.out.println(replaceWords1);
		
		//memecah kata menjadi character
		char[] charReplaceWords1 = replaceWords1.toCharArray();
		System.out.println(charReplaceWords1[17]);
		
		//menampilkan kata muhammad
		String muhammad = words1.substring(6,14); // memanggil huruf berdasarkan index ke 6 dengan batas index ke 14
		System.out.println(muhammad);
		
		//toLowerCase() = untuk mengecilkan semua huruf 
		String lowerWords1 = words1.toLowerCase().replaceAll(" ","");// mengganti kata spasi menjadi tidak spasi SERTA MENGGABUNGKAN FUNGSI LowerCase dengan replace 
		String tempMuhammad = lowerWords1.substring(5,13);
		System.out.println(tempMuhammad);
		
		//mengubah kata (dengan huruf kecil semua) menjadi tipe data char
		char[] tempLowerWords1 = lowerWords1.toCharArray();
		System.out.println(lowerWords1);
		
		//untuk membesarkan semua huruf
		String upperWords1 = words1.toUpperCase();
		System.out.println(upperWords1);

	}

}
