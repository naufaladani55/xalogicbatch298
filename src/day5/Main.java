package day5;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				// jika case 1 terdapat soal 1 maka kerjakan soal 1
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();

		}
	}

	private static void soal1() {
		System.out.println("Disini Soal 1");

		System.out.println("Masukkan jumlah uang Andi: "); // Input uang andi
		int uangAndi = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan Harga Baju (pisahkan menggunakan tanda koma  (,) tanpa spasi ) : "); // Input harga
		String hargaBaju = input.next();
		System.out.println("Masukkan Harga Celana (pisahkan menggunakan tanda koma  (,) tanpa spasi ) : "); // Input
		String hargaCelana = input.next();

		String[] bajuStr = hargaBaju.split(",");
		int[] bajuInt = new int[bajuStr.length];

		for (int i = 0; i < bajuStr.length; i++) {
			bajuInt[i] = Integer.parseInt(bajuStr[i]);
		}

		String[] celanaStr = hargaCelana.split(",");
		int[] celanaInt = new int[celanaStr.length];

		for (int i = 0; i < celanaStr.length; i++) {
			celanaInt[i] = Integer.parseInt(celanaStr[i]);
		}

		int[] gapUang = new int[bajuStr.length];
		int hargaMaks = 0;
		int hargaOutfit = 0;

		for (int i = 0; i < bajuInt.length; i++) {
			for (int j = 0; j < celanaInt.length; j++) {

				hargaOutfit = bajuInt[i] + celanaInt[j];

				if (hargaOutfit <= uangAndi && hargaOutfit > hargaMaks) {
					hargaMaks = hargaOutfit;
				}

			}

		}
		System.out.println(hargaMaks);

	}

	private static void soal2() {
		System.out.println("Masukkan array(pisahkan menggunakan tanda koma  (,) tanpa spasi ) : "); // Input array
		String array = input.next();
		input.nextLine();
		System.out.println("Masukkan jumlah rotasi: "); // Input rotasi
		int rotasi = input.nextInt();

		String[] arrayStr = array.split(",");
		int[] arrayInt = new int[arrayStr.length];
		int[] arrayRotasi1 = new int[arrayStr.length];

		for (int i = 0; i < arrayStr.length; i++) {
			arrayInt[i] = Integer.parseInt(arrayStr[i]);
		}

		for (int i = 0; i < rotasi; i++) {
			String result = "";
			int temp = arrayInt[0];
			for (int j = 0; j < arrayInt.length; j++) {
				if (j == arrayInt.length - 1) {
					arrayInt[j] = temp;
					result = result + temp;
				} else {
					arrayInt[j] = arrayInt[j + 1];
					result = result + arrayInt[j] + ",";
				}
			}
			System.out.print((i + 1) + ": " + result);
			System.out.println();
		}

	}

	private static void soal3() { // BLM
		System.out.print("Masukkan total puntung yang dikumpulkan: ");
		int puntungDiKumpul = input.nextInt();
		int satuBatang = 8;
		int sisa = puntungDiKumpul % satuBatang;

		int totalBatang = (int) (puntungDiKumpul / satuBatang);

		System.out.println("1. Total Batang rokok: " + totalBatang + " sisa puntung: " + sisa);

		System.out.println("2. Jumlah batang rokok yang mampu dirangkai: " + totalBatang + ", \ntotal penjualan: "
				+ totalBatang + " x Rp. 500,- = " + "Rp. " + (totalBatang * 500) + ",-");

	}

	private static void soal4() {
		System.out.println("Disini Soal 4");
		// total makanan yang di makan elsa dan dimas = array di jumlahin

		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Total Menu : ");
		int totalMenu = input.nextInt(); // input nilai (total menu)

		System.out.println(); // Enter

		System.out.println("Alergi index ke : ");
		int alergi = input.nextInt(); // input nilai (alergi)

		input.nextLine();
		System.out.println(); // Enter

		System.out.println("Masukan Harga Menu : ");
		String hargaMenu = input.nextLine(); // input nilai (String)

		System.out.println(); // Enter

		System.out.println("Uang Elsa : ");
		int uang = input.nextInt(); // input rotasi (String)

		System.out.println();

		String[] hargaMenuS = hargaMenu.split(","); // misahin index berdasarkan ,

		int[] hargaMenuI = new int[hargaMenuS.length]; // inisialisasi array integer

		int totalHarga = 0;

		if ((totalMenu != hargaMenuI.length) || (alergi > (hargaMenuI.length - 1))) {
			System.out.println("tidak sesuai inputan");
			return;
		}

		for (int i = 0; i < totalMenu; i++) { // panjang array
			hargaMenuI[i] = Integer.parseInt(hargaMenuS[i]); // convert
			totalHarga = totalHarga + hargaMenuI[i]; // total harga
		}
		totalHarga = totalHarga - hargaMenuI[alergi];
		totalHarga = totalHarga / 2;

		System.out.println("Elsa Harus Membayar : " + (totalHarga));

		if (totalHarga < uang) { // 18000 < 20000
			System.out.println("Lunas dengan kembalian : " + (uang - totalHarga));
		} else if (totalHarga == uang) {
			System.out.println("Uang pas");
		} else {
			System.out.println("Uang Anda Kurang : " + (uang - totalHarga));
		}
	}

	private static void soal5() {
		System.out.println("Disini Soal 5");
		System.out.println("Masukkan Kalimat yang akan dipisahkan: ");
		String kalimat = input.nextLine();
		char[] kalimatGabungan = (kalimat.replaceAll(" ", "")).toLowerCase().toCharArray();
		String vokal = "";
		String konsonan = "";
		for (int i = 0; i < kalimatGabungan.length; i++) {

			if (kalimatGabungan[i] == 'a' || kalimatGabungan[i] == 'i' || kalimatGabungan[i] == 'u'
					|| kalimatGabungan[i] == 'e' || kalimatGabungan[i] == 'o') {
				vokal += kalimatGabungan[i];
			} else {
				konsonan += kalimatGabungan[i];
			}
		}

		char[] vokalArr = vokal.toCharArray();
		char[] konsonanArr = konsonan.toCharArray();

//		sorting vokal
		char temp = 0;
		for (int i = 0; i < vokalArr.length; i++) {
			for (int j = 0; j < vokalArr.length; j++) {
				if ((int) vokalArr[i] <= (int) vokalArr[j]) {
					temp = vokalArr[i];
					vokalArr[i] = vokalArr[j];
					vokalArr[j] = temp;
				}
			}
		}

		for (int i = 0; i < konsonanArr.length; i++) {
			for (int j = 0; j < konsonanArr.length; j++) {
				if ((int) konsonanArr[i] <= (int) konsonanArr[j]) {
					temp = konsonanArr[i];
					konsonanArr[i] = konsonanArr[j];
					konsonanArr[j] = temp;
				}
			}
		}

		System.out.println("Vokal    : " + String.valueOf(vokalArr));
		System.out.println("Konsonan : " + String.valueOf(konsonanArr));

	}

	private static void soal6() {
		System.out.println("Disini Soal 6");
		System.out.println("Masukkan Kalimat yang akan disensor: ");
		String kalimat = input.nextLine();
		String[] kata = kalimat.split(" ");

		for (int i = 0; i < kata.length; i++) {
			char[] karakter = kata[i].toCharArray();
			for (int j = 0; j < karakter.length; j++) {
				if (j % 2 == 1) {
					System.out.print("*");
				} else {
					System.out.print(karakter[j]);
				}

			}
			if (i == kata.length - 1) {

			} else {
				System.out.print(" ");
			}
		}
	}
}
